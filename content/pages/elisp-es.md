Author: David Arroyo Menéndez
Date: 2018-05-21
ISBN: 978-84-608-5935-2
Img: img/elisp-es.png
Pages: 265
Price: 15 €
Save_as: elisp-es.html
Short_title: Emacs Lisp
Url: elisp-es.html
Title: Una Introducción a la programación en Emacs Lisp

Este es el primer libro que editamos. La traducción del libro ha sido
hecha por Libremanuals. Es un libro muy paradigmático de la comunidad
(emacs-es) donde los hispanohablantes se acercan a realizar impulsos de
traducción o lectura del libro para ser mejores usuarios y programadores
de emacs.

En el mundo de la programación hay dos familias: lobos y corderos. Los
lenguajes cordero son lenguajes académicos con belleza matemática,
adecuados para el aprendizaje y/o la investigación; Haskell, Scheme y
Modula son así. Los lenguajes lobo son aptos para la industria, para
ganar dinero; PHP, C y Java son así. Emacs lisp es un lobo con piel de
cordero, usa la belleza científica para aumentar la productividad del
programador.

El autor es Robert J. Chassell, con un prefacio de Richard M. Stallman.
Robert J Chassell es miembro fundador de la Free Software Foundation y
gran orador de ponencias de Software Libre. Richard M. Stallman es
fundador de GNU y de la FSF, así mismo, es autor de GNU Emacs.

Aprender Emacs Lisp, al menos, le permitirá crear pequeñas extensiones
para un .emacs. Para leer este libro no son necesarios conocimientos
anteriores de programación. Os presentamos estos [ejemplos de
configuraciones y extensiones en emacs
lisp](https://github.com/davidam/davidam)

Pedidos a través de:

-   [UAM](https://www.libros.so/libro/una-introduccion-a-la-programacion-en-emacs-lisp_1090951)
-   [Malatesta](http://www.lamalatesta.net/product_info.php/products_id/5852)
-   [Traficantes de
    Sueños](http://traficantes.net/libros/una-introduccion-la-programacion-en-emacs-lisp)
-   [FAL](http://fal.cnt.es/tienda/node/705)
-   [La Rosa
    Negra](http://larosanegraediciones.com/home/885-una-introduccion-a-la-programacion-en-emacs-lisp.html)
-   [Ediciones Fantasma](https://edicionesydistribucionesfantasma.wordpress.com/2018/01/13/una-introduccion-a-la-programacion-en-emacs-lisp/)

Se pueden solicitar mejoras al libro a través de
[Savannah](https://savannah.nongnu.org/projects/elisp-es/). Si quieres
el libro gratis puedes [descargar el paquete
debian](http://davidam.com/debian/elisp-es_1.0-1_all.deb) e instalarlo
en tu sistema basado en debian gnu/linux preferido.

-   ISBN: 978-84-608-5935-2
-   Número de páginas: 265
-   Precio: 15€

Puedes discutir dudas o detalles del libro desde la lista de
distribución [elisp-es](https://lists.riseup.net/www/info/elisp-es)
