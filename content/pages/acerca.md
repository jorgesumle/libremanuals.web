Author: David Arroyo Menéndez
Title: Acerca
Save_as: acerca.html
Url: acerca.html

Libremanuals es un colectivo editorial que surge para la difusión de
libros libres impresos acerca de Software Libre en la lengua española.

Así, se comparte la visión general del proyecto GNU y, en particular,
los siguientes textos:

-   [Por qué el software libre necesita documentación
    libre](https://www.gnu.org/philosophy/free-doc.es.html)
-   [El derecho a
    leer](https://www.gnu.org/philosophy/right-to-read.es.html)
-   [El peligro de los libros
    electrónicos](https://www.gnu.org/philosophy/the-danger-of-ebooks.es.html)

Para decirlo en pocas palabras, para que el derecho a leer no esté en
peligro, necesitamos leer documentos libres. Este colectivo editorial
trata de fomentar este derecho.

La manera que tiene Libremanuals de fomentar este derecho es vender y
traducir libros libres.

También podemos encontrar inspiración positiva a un mundo de
documentación libre en:

-   [Wikipedia](https://www.wikipedia.org/)
-   [Imagina que la Bibliografía Básica de la UNED fuera
    libre](http://www.davidam.com/docu/imagina-bibliografia-libre.html)
-   [Cómo reconocer si un libro es adecuado a tus necesidades
    académicas]({filename}/pages/libros.md)
-   [La perspectiva libertaria de
    Libremanuals]({filename}/pages/perspectiva-libertaria.md)
-   [Por ética todo el software debería ser
    libre](http://www.onemagazine.es/david-arroyo-menendez-software-libre-hackmeeting-libros-gnu-richard-stallman)
-   [Preguntas Frecuentes de Documentación
    Libre]({filename}/pages/faq-freedoc.md)

Puedes estar al tanto de nuestras novedades suscribiéndote a nuestra
[lista de distribución](https://lists.riseup.net/www/info/libremanuals).

Los puntos físicos de distribución de los libros son:

-   [Universidad Autónoma de
    Madrid](http://www.ommcampuslibros.com/librerias/), C/ Einstein, 7
    Edificio Plaza Mayor, Campus de Cantoblanco, 28049 Madrid
-   [Universidad Rey Juan
    Carlos](http://www.ommcampuslibros.com/librerias/), Camino del
    Molino, s/n, 28943 Fuenlabrada, Madrid
-   [La Fuga](https://www.nodo50.org/lafuga/): C/ Conde de Torrejón, 4.
    Sevilla
-   [FAL](http://fal.cnt.es/): C/ de las Peñuelas, 41. Madrid
-   [Librería Malatesta](http://www.lamalatesta.net/): C/ Jesús y
    María, 24. Madrid
-   [Traficantes de Sueños](https://www.traficantes.net/): C/ del Duque
    de Alba, 13. Madrid
-   [La Rosa Negra](http://larosanegraediciones.com/): C/ Santa
    Julia, 6. Madrid

Algunos lugares dónde hemos estado o estaremos presentes son:

-   [I Love Free Software
    2018](https://www.medialab-prado.es/actividades/ilovefs18)
-   [Ubucon 2018](http://ubucon.org/en/events/ubucon-europe/schedule/)
-   [OpenExpoEurope
    2018](https://openexpoeurope.com/es/speaker/david-arroyo/)
-   [Compilando Podacsts
    (minuto 1:56)](https://www.youtube.com/watch?v=dpXy78Wzo7M&feature=youtu.be)

[![Creative Commons
License](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

Esta web está bajo una licencia [Creative Commons Attribution 4.0
International License](http://creativecommons.org/licenses/by/4.0/).
